from math import isnan
from pandas import *
from overlap_calc import *
import gffutils
orthos = read_csv('/home/gencpg/gen_cpg/ortho-cpg-cross-v3.csv')
hgenes = read_csv('/home/gencpg/gen_cpg/human-genes-entropy-v4.csv')
mgenes = read_csv('/home/gencpg/gen_cpg/mouse-genes-entropy-v2.csv')
hs2_cpg = load_cpg_bed('/home/evgeny/tau/gen_cpg/hs_liver_nmi.bed')
hs_cpg = load_cpg_tsv('/home/evgeny/tau/gen_cpg/cpg_islands_ucsc.tsv')
mm_cpg = load_cpg_bed('/home/evgeny/tau/gen_cpg/mm_liver_nmi.bed')
hs_ordered = ordered_cpgs(hs2_cpg)
mm_ordered = ordered_cpgs(mm_cpg)
hs_cpgfull=[h['index'] for h in hgenes[['index', 'chrom','tss']].iloc if nmi_tss(str(h['chrom']), h['tss'], hs_ordered)]
mm_cpgfull=[m['index'] for m in mgenes[['index', 'chrom','tss']].iloc if nmi_tss(str(m['chrom']), m['tss'], mm_ordered)]
mmcons=orthos['Mouse-g2']
diffs=[o for o in orthos.iloc if o['g1'] in hs_cpgfull and o['Mouse-g2'] in mm_cpgfull]
len([o for o in orthos.iloc if o['Mouse-g2'] in mm_cpgfull])
len([o for o in orthos.iloc if o['g1'] in hs_cpgfull and (type(o['Mouse-g2'])!=float or not isnan(o['Mouse-g2']))])
gallus_db = gffutils.FeatureDB('gallus.db')
gg_cpg = load_cpg_bed('/home/evgeny/tau/gen_cpg/gg-liver-chipseq_peaks.bed')
gg_ordered = ordered_cpgs(gg_cpg)
gallus_canons = list(g for g in canonical_transcripts(gallus_db, r'/mnt/hddata/2/evgeny/TAU/Gallus_gallus.GRCg6a.dna_sm.toplevel.fa') if g[0].attributes['biotype'][0] == 'protein_coding')
gg_cpgfull=[c[0].attributes['Parent'][0][5:] for c in gallus_canons if nmi_tss(c[0].chrom, c[0].start if c[0].strand=='+' else c[0].end, gg_ordered)]
ggcons=orthos['Chicken-g2']
len([o for o in orthos.iloc if o['g1'] in hs_cpgfull and o['Chicken-g2'] in gg_cpgfull])
len([x for x in ggcons if x in gg_cpgfull])
dr_cpg = load_cpg_bed('/home/evgeny/tau/gen_cpg/dr_liver_nmi.bed')
dr_ordered = ordered_cpgs(dr_cpg)
dr_db = gffutils.FeatureDB('dr.db')
dr_canons = list(canonical_transcripts(dr_db, r'./Danio_rerio.GRCz11.dna_sm.primary_assembly.fa'))
drcons=orthos['Zebrafish-g2']
dr_cpgfull=[c[0].attributes['Parent'][0][5:] for c in dr_canons if nmi_tss(c[0].chrom, c[0].start if c[0].strand=='+' else c[0].end, dr_ordered)]
anocar_db = gffutils.FeatureDB('anocar.db')
anocar_canons = list(canonical_transcripts(anocar_db, r'./Anolis_carolinensis.AnoCar2.0.dna.toplevel.fa'))
anocar_cpg = load_cpg_bed('/home/evgeny/tau/gen_cpg/anocar_liver_nmi.bed')
anocar_ordered = ordered_cpgs(anocar_cpg)
anocar_cpgfull=[c[0].attributes['Parent'][0][5:] for c in anocar_canons if nmi_tss(c[0].chrom, c[0].start if c[0].strand=='+' else c[0].end, anocar_ordered)]
len(anocar_cpgfull)
len([o for o in orthos.iloc if o['g1'] in hs_cpgfull and o['Lizard-g2'] in anocar_cpgfull])
anocons=orthos['Lizard-g2']
len([x for x in anocons if x in anocar_cpgfull])
len([x for x in ggcons if x in gg_cpgfull])
len([o for o in orthos.iloc if o['g1'] in hs_cpgfull and o['Chicken-g2'] in gg_cpgfull])
len([x for x in drcons if x in dr_cpgfull])
len([o for o in orthos.iloc if o['g1'] in hs_cpgfull and o['Zebrafish-g2'] in dr_cpgfull])
len([x for x in mmcons if x in mm_cpgfull])
len([o for o in orthos.iloc if o['g1'] in hs_cpgfull and o['Mouse-g2'] in mm_cpgfull])


#Our method of CpG-ness
import pickle
hs_overlap = pickle.load(open('ucsc_overlaps_300.pkl', 'rb'))

anocar_overlap = cpg_overlap(anocar_canons, anocar_cpg, pre_tss=300, post_tss=100)
len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Lizard-g2']) is str and anocar_overlap['gene:'+o['Lizard-g2']] > 0.5])
min(len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Lizard-g2']) is str]),len([o for o in orthos.iloc if type(o['Lizard-g2']) is str and 'gene:'+o['Lizard-g2'] in anocar_overlap and anocar_overlap['gene:'+o['Lizard-g2']] > 0.5]))
dr_overlap = cpg_overlap(dr_canons,dr_cpg,pre_tss=300,post_tss=100)
len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Zebrafish-g2']) is str and 'gene:'+o['Zebrafish-g2'] in dr_overlap and dr_overlap['gene:'+o['Zebrafish-g2']] > 0.5])
min(len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Zebrafish-g2']) is str]),len([o for o in orthos.iloc if type(o['Zebrafish-g2']) is str and 'gene:'+o['Zebrafish-g2'] in dr_overlap and dr_overlap['gene:'+o['Zebrafish-g2']] > 0.5]))
gg_overlap = cpg_overlap(gallus_canons,gg_cpg,pre_tss=300,post_tss=100)
len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Chicken-g2']) is str and gg_overlap['gene:'+o['Chicken-g2']] > 0.5])
min(len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Chicken-g2']) is str]),len([o for o in orthos.iloc if type(o['Chicken-g2']) is str and 'gene:'+o['Chicken-g2'] in gg_overlap and gg_overlap['gene:'+o['Chicken-g2']] > 0.5]))
mm_canons = list(canonical_transcripts(gffutils.FeatureDB('mouse.db'), r'/mnt/hddata/2/evgeny/TAU/Mus_musculus.GRCm38.dna_sm.primary_assembly.fa'))
mm_overlap = cpg_overlap(mm_canons,mm_cpg,pre_tss=300,post_tss=100)
len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Mouse-g2']) is str and mm_overlap['gene:'+o['Mouse-g2']] > 0.5])
min(len([o for o in orthos.iloc if 'gene:'+o['g1'] in hs_overlap and  hs_overlap['gene:'+o['g1']]>0.5 and type(o['Mouse-g2']) is str]),len([o for o in orthos.iloc if type(o['Mouse-g2']) is str and 'gene:'+o['Mouse-g2'] in mm_overlap and mm_overlap['gene:'+o['Mouse-g2']] > 0.5]))

for first in ['Human', 'Mouse', 'Lizard', 'Zebrafish', 'Chicken']:
    for second in ['Human', 'Mouse', 'Lizard', 'Zebrafish', 'Chicken']:
        conserved=orthos[[not isnan(x) and not isnan(y) for x,y in orthos[[first+'-CpG-ness', second+'-CpG-ness']].iloc]]
        for first_cpgness in [False, True]:
            for second_cpgness in [False, True]:
                print('%s (%s) - %s (%s): %f'%(first, 'CpG-full' if first_cpgness else 'CpG-less', second, 'CpG-full' if second_cpgness else 'CpG-less',
                                               len(conserved[(conserved[first+'-CpG-ness']>0.5 if first_cpgness else conserved[first+'-CpG-ness']<0.5)&(conserved[second+'-CpG-ness']>0.5 if second_cpgness else conserved[second+'-CpG-ness']<0.5)])/len(conserved[conserved[first+'-CpG-ness']>0.5 if first_cpgness else conserved[first+'-CpG-ness']<0.5])*100))
                
elife_cpgfull = { 'Human' : hs_cpgfull, 'Mouse' : mm_cpgfull, 'Chicken' : gg_cpgfull, 'Lizard' : anocar_cpgfull, 'Zebrafish' : dr_cpgfull }
gene_col = { 'Human' : 'g1', 'Mouse' : 'Mouse-g2', 'Chicken' : 'Chicken-g2', 'Lizard' : 'Lizard-g2', 'Zebrafish' : 'Zebrafish-g2' }
for first in ['Human', 'Mouse', 'Lizard', 'Zebrafish', 'Chicken']:
    for second in ['Human', 'Mouse', 'Lizard', 'Zebrafish', 'Chicken']:
        conserved=orthos[[not isnan(x) and not isnan(y) for x,y in orthos[[first+'-CpG-ness', second+'-CpG-ness']].iloc]]
        for first_cpgness in [False, True]:
            for second_cpgness in [False, True]:
                print('%s (%s) - %s (%s): %f'%(first, 'CpG-full' if first_cpgness else 'CpG-less', second, 'CpG-full' if second_cpgness else 'CpG-less',
                                               len(conserved[[(x in elife_cpgfull[first]) == first_cpgness and (y in elife_cpgfull[second]) == second_cpgness for x,y in conserved[[gene_col[first], gene_col[second]]].iloc]])/len(conserved[[(x in elife_cpgfull[first]) == first_cpgness for x in conserved[gene_col[first]].iloc]])*100))
                
                
                
