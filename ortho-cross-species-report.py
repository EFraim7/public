#!/usr/bin/python3
import gffutils
import pickle
from csv import reader
from overlap_calc import *
mouse_db = gffutils.FeatureDB('mouse.db')
mouse_canons = list(g for g in gffutils.helpers.canonical_transcripts(mouse_db, r'/mnt/hddata/2/evgeny/TAU/Mus_musculus.GRCm38.dna_sm.primary_assembly.fa') if g[0].attributes['biotype'][0] == 'protein_coding')
cpgs_mouse = load_cpg_gff(mouse_db)
mouse_overlap = cpg_overlap(mouse_canons, cpgs_mouse, pre_tss=300, post_tss=100)
gallus_db = gffutils.FeatureDB('gallus.db')
gallus_canons = list(g for g in canonical_transcripts(gallus_db, r'/mnt/hddata/2/evgeny/TAU/Gallus_gallus.GRCg6a.dna_sm.toplevel.fa') if g[0].attributes['biotype'][0] == 'protein_coding')
cpgs_gallus = load_cpg_bed('/home/evgeny/tau/gen_cpg/gg-liver-chipseq_peaks.bed')
gallus_overlap = cpg_overlap(gallus_canons, cpgs_gallus, pre_tss=300, post_tss=100)
anocar_db = gffutils.FeatureDB('anocar.db')
anocar_canons = list(canonical_transcripts(anocar_db, r'./Anolis_carolinensis.AnoCar2.0.dna.toplevel.fa'))
cpgs_anocar = load_cpg_bed('/home/evgeny/tau/gen_cpg/anocar_liver_nmi.bed')
anocar_overlap = cpg_overlap(anocar_canons, cpgs_anocar, pre_tss=300, post_tss=100)
dr_db = gffutils.FeatureDB('dr.db')
dr_canons = list(canonical_transcripts(dr_db, r'./Danio_rerio.GRCz11.dna_sm.primary_assembly.fa'))
cpgs_dr = load_cpg_bed('/home/evgeny/tau/gen_cpg/dr_testes_nmi.bed')
hs_overlap = pickle.load(open('ucsc_overlaps_300.pkl', 'rb'))
dr_overlap = cpg_overlap(dr_canons, cpgs_dr, pre_tss=300, post_tss=100)
from pandas import DataFrame
from itertools import *
species = [('ENSDARG', 'Zebrafish'), ('ENSGALG', 'Chicken'), ('ENSG0', 'Human'), ('ENSMUSG', 'Mouse'), ('ENSACAG', 'Lizard')]
def genespecie(g):
    for s in species:
        if g.startswith(s[0]):
            return s[1]
    return None

orthos=DataFrame(data=None, columns=['%s-%s'%c for c in product(['Lizard','Zebrafish','Mouse','Human','Chicken'],['g2','CpG-ness'])]+['CpG-ness','g1']).set_index('g1')
from pandas import Series
for r in reader(open('/data/db/one2one.csv')):
  if genespecie(r[0]) == 'Human' and genespecie(r[2]) != None:
    if r[0] not in orthos.index:
      orthos=orthos.append(Series({genespecie(r[2])+'-g2' : r[2]}, name=r[0]))
    else:
      orthos.at[r[0], genespecie(r[2])+'-g2'] = r[2]

orthos=orthos.assign(**{'Lizard-CpG-ness': ('gene:'+orthos['Lizard-g2']).map(anocar_overlap)})
orthos=orthos.assign(**{'Zebrafish-CpG-ness': ('gene:'+orthos['Zebrafish-g2']).map(dr_overlap)})
orthos=orthos.assign(**{'Chicken-CpG-ness': ('gene:'+orthos['Chicken-g2']).map(gallus_overlap)})
orthos=orthos.assign(**{'Mouse-CpG-ness': ('gene:'+orthos['Mouse-g2']).map(mouse_overlap)})
orthos=orthos.assign(**{'Human-g1' : orthos.index })
orthos=orthos.assign(**{'Human-g2' : orthos.index })
orthos=orthos.assign(**{'Human-CpG-ness': ('gene:'+orthos['Human-g2']).map(hs_overlap)})
orthos.drop(columns=['Human-g1','Human-g2','CpG-ness']).to_csv('ortho-cpg-cross-v4.csv')
