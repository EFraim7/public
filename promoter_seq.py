
def load_promoter_seq(g, fasta, len):
    if g.strand == '+':
        return fasta.get_seq(g.chrom, max(g.start-len, 0), max(g.start-len, 0)+min(len, g.start), rc=False)
    else:
        return fasta.get_seq(g.chrom, g.end, g.end+len, rc=True)

match_dic = {}
match_dic[('A', 'A')] = 2
match_dic[('A', 'C')] = -2
match_dic[('A', 'T')] = -2
match_dic[('A', 'G')] = -1
match_dic[('C', 'A')] = -2
match_dic[('C', 'C')] = 2
match_dic[('C', 'T')] = -1
match_dic[('C', 'G')] = -2
match_dic[('G', 'A')] = -1
match_dic[('G', 'C')] = -2
match_dic[('G', 'T')] = -2
match_dic[('G', 'G')] = 2
match_dic[('T', 'A')] = -2
match_dic[('T', 'C')] = -1
match_dic[('T', 'T')] = 2
match_dic[('T', 'G')] = -2
match_dic[('N', 'A')] = -2
match_dic[('N', 'C')] = -2
match_dic[('N', 'T')] = -2
match_dic[('N', 'G')] = -2
match_dic[('A', 'N')] = -2
match_dic[('C', 'N')] = -2
match_dic[('T', 'N')] = -2
match_dic[('G', 'N')] = -2
    
def align_promoters(seq_g):
    from Bio import pairwise2
    return pairwise2.align.localds(seq_g[0], seq_g[1], match_dic, -8, -0.01)[0]

import pickle
from pyfaidx import Fasta
import Bio.Align
import numpy as np
from multiprocessing import Pool

class PromoterSeqAnalysis(object):
    def __init__(self, prefix, export_prefix, fasta, seq_len):
        self.fasta = Fasta(fasta, as_raw=False)
        self.canons = pickle.load(open('/data/db/import/save/'+export_prefix+'-canons.pkl', 'rb'))
        self.export = pickle.load(open('/data/db/import/save/'+export_prefix+'-export.pkl', 'rb'))
        self.prefix = prefix
        self.seq_len = seq_len

    def do(self):
        p=Pool(8)
        genes = [(load_promoter_seq(self.canons['gene:'+r['g1']][0], self.fasta, self.seq_len).seq.upper(),
                  load_promoter_seq(self.canons['gene:'+r['g2']][0], self.fasta, self.seq_len).seq.upper()) for r in self.export.iloc]
        alignments = p.map(align_promoters, genes, chunksize=16384)
        self.alignments = list(zip(((r['g1'], r['g2']) for r in self.export.iloc), alignments))
        self.export = self.export.assign(PromPresScore300=[a[2] for a in alignments],
                                         PromPresTssMinus300=[min(len(a[0][a[4]:])-a[0][a[4]:].count('-'), len(a[1][a[4]:])-a[1][a[4]:].count('-')) for a in alignments],
                                         PromPresShift300=[abs(len(a[0][a[4]:])-a[0][a[4]:].count('-')-len(a[1][a[4]:])-a[1][a[4]:].count('-')) for a in alignments])
        pickle.dump(self.export, open('/data/db/import/save/'+self.prefix+'-export-sim-local.pkl', 'wb'))
    
aligner_hs = PromoterSeqAnalysis('human', 'paralogs', r'/mnt/hddata/2/evgeny/TAU/Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa', 300)
aligner_mm = PromoterSeqAnalysis('mouse', 'mouse', r'/mnt/hddata/2/evgeny/TAU/Mus_musculus.GRCm38.dna_sm.primary_assembly.fa', 300)
if __name__ == '__main__':
    aligner_hs.do()
    aligner_mm.do()
