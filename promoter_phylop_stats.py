
from pandas import *
from overlap_calc import *
import gffutils
import numpy as np
import pickle
import re
import matplotlib.pyplot as plt
from csv import reader, DictReader
from scipy.stats import kstest
import pyBigWig
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
CHROMLEN=250000000
bw = pyBigWig.open("/data/db/import/phylop/hg38.phyloP7way.bw")
db = gffutils.FeatureDB('homo.db')
hgenes = read_csv('/home/gencpg/gen_cpg/human-genes-entropy-v4.csv')
hs_cpg = load_cpg_tsv('/home/evgeny/tau/gen_cpg/cpg_islands_ucsc.tsv')
hs_ordered = ordered_cpgs(hs_cpg)
cpg_full=['gene:'+h['index'] for h in hgenes[['index', 'chrom','tss']].iloc if nmi_tss(str(h['chrom']), h['tss'], hs_ordered)]
cpg_less=['gene:'+h['index'] for h in hgenes[['index', 'chrom','tss']].iloc if not nmi_tss(str(h['chrom']), h['tss'], hs_ordered)]
#overlap = pickle.load(open('ucsc_overlaps_300.pkl', 'rb'))
#cpg_less, cpg_full = [set(s) for s in split_overlaps(overlap)]
canons = canons_by_gene(db, r'/mnt/hddata/2/evgeny/TAU/Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa')

PRE=1000
POST=0

cpglesshist = np.zeros((PRE+POST, len(cpg_less)), dtype='float')
cpgfullhist = np.zeros((PRE+POST, len(cpg_full)), dtype='float')
#for 
for cset, h in ((cpg_less, cpglesshist), (cpg_full, cpgfullhist)):
    j=0
    for ckey in cset:
        c = canons[ckey][0]
        if 'chr'+c.chrom not in bw.chroms():
            continue
        if c.strand == '+':
            h[max(0, PRE-c.start):min(POST+PRE, CHROMLEN-max(0, c.start-PRE)), j] = np.nan_to_num(bw.values('chr'+c.chrom,max(0, c.start-PRE),min(max(0, c.start-PRE)+POST+PRE-max(0, PRE-c.start), CHROMLEN)))
        else:
            h[max(0, POST-c.end):min(POST+PRE, CHROMLEN-max(0, c.end-POST)), j] = np.nan_to_num(bw.values('chr'+c.chrom,max(0, c.end-POST),min(max(0, c.end-POST)+POST+PRE-max(0, POST-c.end), CHROMLEN))[::-1])
        j += 1

for off in range(PRE+POST):
    print(kstest(cpglesshist[off,:], cpgfullhist[off,:], alternative='less'))
