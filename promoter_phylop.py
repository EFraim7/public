
from pandas import *
from overlap_calc import *
import gffutils
import numpy as np
import pickle
import re
import matplotlib.pyplot as plt
from csv import reader, DictReader
import pyBigWig
import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
CHROMLEN=250000000
bw = pyBigWig.open("/data/db/import/phylop/hg38.phyloP7way.bw")
db = gffutils.FeatureDB('homo.db')
hgenes = read_csv('/home/gencpg/gen_cpg/human-genes-entropy-v4.csv')
hs_cpg = load_cpg_tsv('/home/evgeny/tau/gen_cpg/cpg_islands_ucsc.tsv')
hs_ordered = ordered_cpgs(hs_cpg)
cpg_full=['gene:'+h['index'] for h in hgenes[['index', 'chrom','tss']].iloc if nmi_tss(str(h['chrom']), h['tss'], hs_ordered)]
cpg_less=['gene:'+h['index'] for h in hgenes[['index', 'chrom','tss']].iloc if not nmi_tss(str(h['chrom']), h['tss'], hs_ordered)]
#overlap = pickle.load(open('ucsc_overlaps_300.pkl', 'rb'))
#cpg_less, cpg_full = [set(s) for s in split_overlaps(overlap)]
canons = canons_by_gene(db, r'/mnt/hddata/2/evgeny/TAU/Homo_sapiens.GRCh38.dna_sm.primary_assembly.fa')

PRE=1000
POST=0

cpglesshist = np.zeros(PRE+POST, dtype='float')
cpgfullhist = np.zeros(PRE+POST, dtype='float')
cpglesserr = np.zeros(PRE+POST, dtype='float')
cpgfullerr = np.zeros(PRE+POST, dtype='float')
#for 
for cset, h in ((cpg_less, cpglesshist), (cpg_full, cpgfullhist)):
    for ckey in cset:
        c = canons[ckey][0]
        if 'chr'+c.chrom not in bw.chroms():
            continue
        if c.strand == '+':
            h[max(0, PRE-c.start):min(POST+PRE, CHROMLEN-max(0, c.start-PRE))] += np.nan_to_num(bw.values('chr'+c.chrom,max(0, c.start-PRE),min(max(0, c.start-PRE)+POST+PRE-max(0, PRE-c.start), CHROMLEN)))
        else:
            h[max(0, POST-c.end):min(POST+PRE, CHROMLEN-max(0, c.end-POST))] += np.nan_to_num(bw.values('chr'+c.chrom,max(0, c.end-POST),min(max(0, c.end-POST)+POST+PRE-max(0, POST-c.end), CHROMLEN))[::-1])

cpglessavg = cpglesshist/float(len(cpg_less))
cpgfullavg = cpgfullhist/float(len(cpg_full))
cpglesserr = np.zeros(PRE+POST, dtype='float')
cpgfullerr = np.zeros(PRE+POST, dtype='float')

for cset, h, mean in ((cpg_less, cpglesserr, cpglessavg), (cpg_full, cpgfullerr, cpgfullavg)):
    for ckey in cset:
        c = canons[ckey][0]
        if 'chr'+c.chrom not in bw.chroms():
            continue
        if c.strand == '+':
            h[max(0, PRE-c.start):min(POST+PRE, CHROMLEN-max(0, c.start-PRE))] += np.power(np.nan_to_num(bw.values('chr'+c.chrom,max(0, c.start-PRE),min(max(0, c.start-PRE)+POST+PRE-max(0, PRE-c.start), CHROMLEN)))-mean, 2.0)
        else:
            h[max(0, POST-c.end):min(POST+PRE, CHROMLEN-max(0, c.end-POST))] += np.power(np.nan_to_num(bw.values('chr'+c.chrom,max(0, c.end-POST),min(max(0, c.end-POST)+POST+PRE-max(0, POST-c.end), CHROMLEN))[::-1])-mean, 2.0)

cpglesserr = np.power(cpglesserr/len(cpg_less), 0.5)
cpgfullerr = np.power(cpgfullerr/len(cpg_full), 0.5)

#plt.errorbar(range(-PRE, POST), cpglessavg, yerr=cpglesserr, errorevery=50, label='CpG-less', capsize=5)
#plt.errorbar(range(-PRE, POST), cpgfullavg, yerr=cpgfullerr, errorevery=50, label='CpG-full', capsize=5)
fig, ax=plt.subplots(figsize=(9,5))
ax.plot(range(-PRE, POST), cpglessavg, label='CpG-less', color='#0B9DCF')
ax.plot(range(-PRE, POST), np.row_stack([cpglessavg-cpglesserr, np.zeros(PRE+POST)]).max(axis=0), color='#0B9DCF', alpha=0.1)
ax.plot(range(-PRE, POST), cpglessavg+cpglesserr, color='#0B9DCF', alpha=0.1)
ax.fill_between(range(-PRE, POST), np.row_stack([cpglessavg-cpglesserr, np.zeros(PRE+POST)]).max(axis=0), cpglessavg+cpglesserr, color='#0B9DCF', alpha=0.2)
ax.plot(range(-PRE, POST), cpgfullavg, label='CpG-full', color='#EF893D')
ax.plot(range(-PRE, POST), np.row_stack([cpgfullavg-cpgfullerr, np.zeros(PRE+POST)]).max(axis=0), color='#EF893D', alpha=0.1)
ax.plot(range(-PRE, POST), cpgfullavg+cpgfullerr, color='#EF893D', alpha=0.1)
ax.fill_between(range(-PRE, POST), np.row_stack([cpgfullavg-cpgfullerr, np.zeros(PRE+POST)]).max(axis=0), cpgfullavg+cpgfullerr, color='#EF893D', alpha=0.2)

plt.legend()
plt.savefig('promoter-phylop-err-human.pdf', dpi=600)

